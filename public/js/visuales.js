$(document).ready(function(){
    $('select').formSelect();
});
var validation = true

function range_validate(s, value, values){
    if(value >= parseFloat(values[0]) && value <= parseFloat(values[1])){
        $("#" + s + "in_range").css("background-color", "#4caf50")
        $("#" + s + "in_range").html("<span>In range</span>")
        validation = true
    }else{
        $("#" + s + "in_range").css("background-color", "#f44336")
        $("#" + s + "in_range").html("<span>Out of range</span>")
        validation = false
    }
}

$('#size, #min_range, #max_range').change(function(){
    var values = [$("#min_range").val(), $("#max_range").val()]
    var size = $("#size").val()
    /* if(values[0] == 0 || values[1] == 0){
        if(values[0] == 0){
            alert("please introduce de minimun volume")
        }
        if(values[1] == 0){
            alert("please introduce de maximun volume")
        }
    }else{ */
        range_validate("", size, values)
    //}
})

var crypto_value
$('#quote_client, #quote_min_range, #quote_max_range, #OSL_Side').change(function(){
    var values = [$("#quote_min_range").val(), $("#quote_max_range").val()]
    var quote = $("#quote_client").val()
    var osl_side = $("#OSL_Side").val()
  /*   if(values[0] == 0 || values[1] == 0){
        if(values[0] == 0){
            alert("please introduce de minimun price")
        }
        if(values[1] == 0){
            alert("please introduce de maximun price")
        }
    }else{ */
        range_validate("quote_", quote, values) 
        var spread
        if(osl_side == "sell"){
            spread = ((((parseFloat(quote) / parseFloat(crypto_value)) - 1) * 100)  * 100).toFixed(2)
        }else if (osl_side == "buy") {
            spread = ((1 - (parseFloat(quote) / parseFloat(crypto_value))) * 10000).toFixed(2)
        }
        console.log(spread);
        $("#lbSpread").addClass("active")
        $("#Spread").val(spread)

        if(!($("#price").val() == ""))
            console.log(osl_side == "sell");
            console.log(osl_side);
            side = osl_side == "sell"
            alert(`You can ${side ? "sell: " : "buy: "} ${$("#size").val()} ${$("#coin").val()} at ${$("#price").val()}`)
    //}
})

var link = "https://api.kraken.com/0/public/Ticker?pair="
var poner_precio = (data, fiat, crypto) => {
    if(data["e"]){
        alert("Error on change type")
    } else {
        var message, prize;
        if(crypto == "BTC"){
            if(fiat == "EUR"){
                crypto_value = data["result"]["XXBTZEUR"]["c"][0]
                prize = data["result"]["XXBTZEUR"]["c"][0]
                message = "€" + prize
            } else {
                crypto_value = data["result"]["XXBTZUSD"]["c"][0]
                prize = data["result"]["XXBTZUSD"]["c"][0]
                message = "$" + prize + " USD"
            }
        }else if(crypto == "ETH"){
            if(fiat == "EUR"){
                crypto_value = data["result"]["XETHZEUR"]["c"][0]
                prize =  data["result"]["XETHZEUR"]["c"][0]
                message = "€" + prize
            } else {
                crypto_value = data["result"]["XETHZUSD"]["c"][0]
                prize = data["result"]["XETHZUSD"]["c"][0]
                message = "$" + prize + " USD"
            }
        }else if(crypto == "USDT"){
            if(fiat == "EUR"){
                crypto_value = data["result"]["USDTEUR"]["c"][0]
                prize = data["result"]["USDTEUR"]["c"][0]
                message = "€" + prize
            } else {
                crypto_value = data["result"]["USDTUSD"]["c"][0]
                prize = data["result"]["USDTUSD"]["c"][0]
                message = "$" + prize + " USD"
            }
        }else if(crypto == "USDC"){
            if(fiat == "EUR"){
                crypto_value = data["result"]["USDCEUR"]["c"][0]
                prize = data["result"]["USDCEUR"]["c"][0]
                message = "€" + prize
            } else {
                crypto_value = data["result"]["USDCUSD"]["c"][0]
                prize = data["result"]["USDCUSD"]["c"][0]
                message = "$" + prize  + " USD"
            }
        }else if(crypto == "LTC"){
            if(fiat == "EUR"){
                crypto_value = data["result"]["XLTCZEUR"]["c"][0]
                prize = data["result"]["XLTCZEUR"]["c"][0]
                message = "€" + prize
            } else {
                crypto_value = data["result"]["XLTCZUSD"]["c"][0]
                prize = data["result"]["XLTCZUSD"]["c"][0]
                message = "$" + prize + " USD"
            }
        }
        $("#lbPrice").addClass("active")
        $("#price").val(message)

        var min = parseFloat(prize) * 0.9
        var max = parseFloat(prize) * 1.1
        $("#lbQuoteMin").addClass("active")
        $("#quote_min_range").val(min)
        $("#lbQuoteMax").addClass("active")
        $("#quote_max_range").val(max)
    }
    
}

$("#FIAT").on("change", function(){
    var crypto = $("#coin").val()
    var fiat = $(this).val()
    if(fiat != "EUR"){
        fiat = "USD"
    }
    /* if(crypto == 0){
        alert("Please select a coin")
    } else { */

        fetch(link+crypto+fiat).then(
            result => result.json()
        ).then(data => {
            poner_precio(data, fiat, crypto)          
        })
   // }
})

$("#coin").on("change", function(){
    var crypto = $(this).val()
    var fiat =  $("#FIAT").val()
    /* if(fiat == 0){
        alert("Please select a fiat")
    } else { */
        if(fiat != "EUR"){
            fiat = "USD"
        }
        fetch(link+crypto+fiat).then(
            result => result.json()
        ).then(data => {
            poner_precio(data, fiat, crypto)          
        })
    //}
    var min = 0
    var max = 5000
    if(crypto == "BTC"){
        min = 0
        max = 1000
    }else if(crypto == "ETH"){
        min = 0
        max = 5000
    }else if(crypto == "USDT"){
        min = 0
        max = 5000000
    }else if(crypto == "USDC"){
        min = 0
        max = 5000000
    }else if(crypto == "LTC"){
        min = 0
        max = 50000
    }
    
    
    $("#min_range").val(min)
    $("#max_range").val(max)
})